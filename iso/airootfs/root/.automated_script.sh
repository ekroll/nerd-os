#!/usr/bin/env bash
workdir="/tmp/archgen"


function get_repo {
    read -p "Enter URL to open git repo: " url

    if [[ "${url}" =~ ^(https://) ]]; then
        echo "Attempting to clone repository"
        git clone ${url} ${workdir} && return $?
    else
        echo "Invalid URL syntax, use: https://example.com"
    fi

    return 1
}


function main {
    get_repo
    local err=$?

    if [[ ${rt} -eq 0 ]]; then
        chmod +x ${workdir}/install.sh &&
        echo "Running script: ${workdir}/install.sh" ||
        echo "Failed setting script to executable"
        cd ${workdir} && sh install.sh
    else
        echo "Failed, try again"
        main
    fi
}


if [[ $(tty) == "/dev/tty1" ]]; then
    pacman -Sy --noconfirm
    main
fi
